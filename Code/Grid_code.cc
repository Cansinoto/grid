#include "Grid.h"
#include <stdio.h>
#include <stdlib.h>
Grid::Grid(){
    rows_=0;
    cols_=0;
    data_=nullptr;
}
Grid::~Grid(){}
void Grid::init(int rows, int cols){    
    if(Grid::checkElement(rows)==0 && Grid::checkElement(cols)==0){
        rows_=rows;
        cols_=cols;
        if((data_=(int*)malloc(sizeof(int)*rows_*cols_))==NULL){
            printf("Error in init(int int)");
        }
    }
}
void Grid::init(int side){
    if(Grid::checkElement(side)==0){
        rows_=side;
        cols_=side;
        if((data_=(int*)malloc(sizeof(int)*rows_*cols_))==NULL){
            printf("Error in init(int)");
        }
    }
}
void Grid::zeros(){
    for(int i=0;i<rows_*cols_;i++){
        data_[i]=0;
    }
}
void Grid::fill(int number){
    for(int i=0;i<rows_*cols_;i++){
        data_[i]=number;
    }
}
int Grid::getElement(int row, int col){
    if(row<=rows_ && col<=cols_){
        for(int i=0;i<row;i++){
            for(int j=0; j<col;j++){
               return data_[i*cols_+j];
            }
        }
    }
    printf("Error in getElement");
    return 0;
}
void Grid::setElement(int row, int col, int number){
    int num;
    if(row<=rows_ && col<=cols_){
        for(int i=0;i<row;i++){
            for(int j=0; j<col;j++){
               num=i*cols_+j;
            }
        }
    }
    data_[num]=number;
    
}
void Grid::print(){
    for(int i=0;i<rows_;i++){
        for(int j=0;j<cols_;j++){
            if(j==0){
                printf("|");
            }
            if(data_[i*cols_+j]<10 && data_[i*cols_+j]>=0){
                printf(" 0%d |",data_[i*cols_+j]);
            }else{
                printf(" %d |",data_[i*cols_+j]);
        }
        }
        printf("\n");
    }
    printf("\n");
}
void Grid::release(){
    if(data_!=NULL){
        free(data_);
        printf("\nMemory released");
    }else{
        printf("Error in release");
    }
}
int Grid::checkElement(int value){
    if(value<1){
        return 1;
    }else{
        return 0;
    }
}

void Grid::resize(int rows, int cols){
    int *data;
    if(Grid::checkElement(rows)==0 && Grid::checkElement(cols)==0){
        if((data=(int*)calloc(rows*cols,sizeof(int)))==NULL){
                printf("Error in resize");
        }
        for(int i=0;i<rows && i<rows_;i++){
            for(int j=0; j<cols && j<cols_;j++){
               data[i*cols+j]=data_[i*cols_+j];
            }
        }
    
        free(data_);
        if((data_=(int*)malloc(sizeof(int)*rows*cols))==NULL){
            printf("Error in resize");
        }else{
            for(int i=0;i<rows*cols;i++){
                data_[i]=data[i];
            }
        }
        rows_=rows;
        cols_=cols;
    }
}

void Grid::add(class Grid grid_a){
    if(grid_a.rows_==rows_ && grid_a.cols_==cols_){
        for(int i=0;i<rows_*cols_;i++){
            data_[i]+=grid_a.data_[i];
        }
    }
}

void Grid::substract(class Grid grid_a){
    if(grid_a.rows_==rows_ && grid_a.cols_==cols_){
        for(int i=0;i<rows_*cols_;i++){
            data_[i]-=grid_a.data_[i];
        }
    }
}