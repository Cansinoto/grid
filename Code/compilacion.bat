﻿@REM Compilación y Enlace con librería gráfica.
@cls


cl /nologo /GR- /EHs /MD /c Grid_code.cc -I ../include -I .
cl /nologo /GR- /EHs /MD /c Grid_main.cc -I ../include -I .

cl /nologo /GR- /EHs /MD /Fe: ../Grid.exe Grid_code.obj Grid_main.obj D:\Segundo\Lib_Graph\ESAT_rev168\bin\ESAT.lib  opengl32.lib user32.lib gdi32.lib shell32.lib
   
@echo -------------------------------
@echo  Proceso por lotes finalizado.
@echo -------------------------------