#ifndef _GRID_H_
#define _GRID_H_ 1

class Grid {
  public:
    //Constructor
    Grid();
    //Destructor "chirimbolo" 
    ~Grid();
    //Methods
    void init(int rows,int cols);
    void init(int side);
    void zeros();
    void fill(int number);
    int getElement(int row, int col);
    void setElement(int row, int col, int number);
    void print();
    void release(); 
    void resize(int rows,int cols);
    void add(class Grid grid_a);
    void substract(class Grid grid_a);   
  private:
    int checkElement(int value);

    //Data members
    int rows_;
    int cols_;
    int* data_;
};

#endif