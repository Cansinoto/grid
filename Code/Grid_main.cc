#include <stdio.h>
#include <windows.h>
#include "Grid.h"

int main() {
  Grid grid_1,grid_2,grid_3;

  grid_1.init(10,10);
  grid_1.fill(10);
  grid_2.init(5,5);
  grid_2.fill(6);
  grid_3.init(5,5);
  grid_3.fill(2);
  printf("Grid 1\n");
  grid_1.print();
  printf("Grid 2\n");
  grid_2.print();
  printf("Grid 3\n");
  grid_3.print();  
  grid_1.setElement(4,3,1);
  grid_1.setElement(2,1,12);
  grid_1.setElement(2,2,3);
  grid_1.setElement(4,5,8);
  printf("Grid 1 \n");
  grid_1.print(); 
  grid_1.resize(5,5); 
  printf("Grid 1 size 5*5\n");
  grid_1.print();
  grid_1.add(grid_2);
  printf("Grid 1 + Grid 2\n");
  grid_1.print();
  grid_1.substract(grid_3);
  printf("Grid 1 - Grid 3\n");
  grid_1.print();
  system("pause");
  
  return 0;
}